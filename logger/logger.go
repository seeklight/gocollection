package logger

import (
	"flag"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
)

// SetupLogrus ...
// @ string projectName: The project name
func SetupLogrus(projectName string) {

	var err error
	var logFile *os.File
	var timestamp = time.Now()
	var absolutePath string
	var logName string
	var logDir string
	var logPath string

	// Move this part into a common flag manager that handle envs a save the debug information there
	debug := flag.Bool("debug", false, "Whether debug mode has to be enabled.")
	flag.Parse()
	if *debug {
		logrus.SetLevel(logrus.DebugLevel)
		absolutePath, _ = os.Getwd()
	} else {
		absolutePath, err = filepath.Abs(filepath.Dir(os.Args[0]))
		if err != nil {
			logrus.Warn(err)
		}
	}
	logName = timestamp.Format("20060102") + "_"
	logName += strings.ToLower(projectName)
	logName = strings.Replace(logName, " ", "_", -1)
	logDir = absolutePath + "/" + "log"
	logPath = logDir + "/" + logName + ".log"

	log.Println(logPath)

makeLogDir:
	logFile, err = os.OpenFile(logPath, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		logrus.Warnf("Couldn't open log-file, falling back to stdout: %s", err)
		if os.IsNotExist(err) {
			logrus.Info("Trying to create log folder...")
			err = os.Mkdir(logDir, os.FileMode(0700))
			if err != nil {
				logrus.Warnf("Error while creating the folder: %s", err)
			}
			goto makeLogDir
		}
	} else {
		logrus.SetFormatter(&logrus.JSONFormatter{})
		logrus.SetOutput(logFile)
		defer func(f *os.File) {
			f.Sync() //look here
			f.Close()
		}(logFile)
	}

	logrus.Info(projectName, " started")
}

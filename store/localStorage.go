package store

import (
	"github.com/patrickmn/go-cache"
)

var storage *cache.Cache

// InitDefaultStorage ...
func InitDefaultStorage() {
	storage = cache.New(cache.NoExpiration, cache.NoExpiration)
}

// SetStorageValue ...
func SetStorageValue(key string, value interface{}) {
	storage.Set(key, value, cache.NoExpiration)
}

// GetStorageValue ...
func GetStorageValue(key string) interface{} {
	val, found := storage.Get(key)
	if found {
		return val
	}
	return nil
}

func ShowItems() map[string]cache.Item {
	return storage.Items()
}
